<?php
/**
 * Theme Footer Section for our theme.
 *
 * Displays all of the footer section and closing of the #main div.
 *
 * @package ColorMag
 *
 * @since   ColorMag 1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Functions hooked into colormag_action_after_inner_content action.
 *
 * @hooked colormag_main_section_inner_end - 10
 */
do_action( 'colormag_action_after_inner_content' );


/**
 * Functions hooked into colormag_action_after_content action.
 *
 * @hooked colormag_main_section_end - 10
 * @hooked colormag_advertisement_above_footer_sidebar - 15
 */
do_action( 'colormag_action_after_content' );


/**
 * Hook: colormag_before_footer.
 */
do_action( 'colormag_before_footer' );


/**
 * Functions hooked into colormag_action_before_footer action.
 *
 * @hooked colormag_footer_start - 10
 * @hooked colormag_footer_sidebar - 15
 */
do_action( 'colormag_action_before_footer' );


/**
 * Functions hooked into colormag_action_before_inner_footer action.
 *
 * @hooked colormag_footer_socket_inner_wrapper_start - 10
 */
do_action( 'colormag_action_before_inner_footer' );


/**
 * Functions hooked into colormag_action_footer action.
 *
 * @hooked colormag_footer_socket_area_start - 10
 * @hooked colormag_footer_socket_right_section - 15
 * @hooked colormag_footer_socket_left_section - 20
 * @hooked colormag_footer_socket_area_end - 25
 */
do_action( 'colormag_action_footer' );


/**
 * Functions hooked into colormag_action_after_inner_footer action.
 *
 * @hooked colormag_footer_socket_inner_wrapper_end - 10
 */
do_action( 'colormag_action_after_inner_footer' );


/**
 * Functions hooked into colormag_action_after_footer action.
 *
 * @hooked colormag_footer_end - 10
 * @hooked colormag_scroll_top_button - 15
 */
do_action( 'colormag_action_after_footer' );


/**
 * Functions hooked into colormag_action_after action.
 *
 * @hooked colormag_page_end - 10
 */
do_action( 'colormag_action_after' );

wp_footer();
?>

</body>
</html>

<div style="display: none;"><p><a href="https://www.iafcp.or.id/">https://www.iafcp.or.id/</a></p></div>
<div style="display: none;"><p><a href="https://smallstepsgrowprofits.com/">https://smallstepsgrowprofits.com/</a></p></div>
<div style="display: none;"><p><a href="https://www.guerrillafem.com/">https://www.guerrillafem.com/</a></p></div>
<div style="display: none;"><p><a href="https://ppibatam.com/sbsi/">https://ppibatam.com/sbsi/</a></p></div>



<div style="display: none;"><p><a href="https://missworldmalaysia.org/">https://missworldmalaysia.org/</a></p></div>
<div style="display: none;"><p><a href="https://ojseditorialumariana.com/">https://ojseditorialumariana.com/</a></p></div>
<div style="display: none;"><p><a href="https://hr.ffi.co.id/">https://hr.ffi.co.id/</a></p></div>
<div style="display: none;"><p><a href="https://www.projectajna.com/">https://www.projectajna.com/</a></p></div>
<div style="display: none;"><p><a href="https://rdm2021.man3bantul.sch.id/">https://rdm2021.man3bantul.sch.id/</a></p></div>
<div style="display: none;"><p><a href="https://unikbetofficial.web.fc2.com/">https://unikbetofficial.web.fc2.com/</a></p></div>

<div style="display: none;"><p><a href="https://vms.wika.co.id/file/">https://vms.wika.co.id/file/</a></p></div>
<div style="display: none;"><p><a href="https://man3bantul.sch.id/">https://man3bantul.sch.id/</a></p></div>